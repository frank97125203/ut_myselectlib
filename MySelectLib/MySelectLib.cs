﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySelectLib
{
    public static class MySelectLib
    {
        public static IEnumerable<TResult> MySelect<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {

            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (selector == null)
            {
                throw new ArgumentNullException("selector");
            }

            return InternalSelect(source, selector);
        }

        private static IEnumerable<TResult> InternalSelect<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            foreach (var item in source)
            {
                yield return selector(item);
            }
        }

        public static IEnumerable<TResult> MySelect<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (selector == null)
            {
                throw new ArgumentNullException("selector");
            }

            return InternalSelect(source, selector);
        }

        private static IEnumerable<TResult> InternalSelect<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
        {
            var index = 0;
            foreach (var item in source)
            {
                yield return selector(item, index);
                index++;
            }
        }
    }
}
