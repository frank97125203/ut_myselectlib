using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MySelectLib
{
    public class MySelectLibTest
    {
        List<Person> people;
        [SetUp]
        public void Setup()
        {
            people = GetPeople();
        }

        [Test]
        public void ObjectCompare()
        {
            var expected = people.Select(person => new { person.Name, person.Age }).ToList();
            var result = people.MySelect(person => new { person.Name, person.Age }).ToList();
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void ObjectAgeCompare()
        {
            var expected = people.Select(person => new { person.Age }).ToList();
            var result = people.MySelect(person => new { person.Age }).ToList();
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void ObjectNameCompare()
        {
            var expected = people.Select(person => new { person.Name }).ToList();
            var result = people.MySelect(person => new { person.Name }).ToList();
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void ObjectWithIndexCompare()
        {
            var expected = people.Select((person, index) => new { index, person.Name, person.Age }).ToList();
            var result = people.MySelect((person, index) => new { index, person.Name, person.Age }).ToList();
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void ObjectAgeWithIndexCompare()
        {
            var expected = people.Select((person, index) => new { index, person.Age }).ToList();
            var result = people.MySelect((person, index) => new { index, person.Age }).ToList();
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void ObjectNameWithIndexCompare()
        {
            var expected = people.Select((person, index) => new { index, person.Name }).ToList();
            var result = people.MySelect((person, index) => new { index, person.Name }).ToList();
            Assert.AreEqual(expected, result);
        }
        private static List<Person> GetPeople()
        {
            return new List<Person>
            {
                new() {Name = "James", Age = 17},
                new() {Name = "CC", Age = 19},
                new() {Name = "Frank", Age = 20}
            };
        }
    }
    public struct Person
    {
        public string Name { get; init; }
        public int Age { get; init; }
    }
}